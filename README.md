### Pro Git - 基础 ###

* 配置文件
* /etc/gitconfig : 用 git config --system 写
* /.gitconfig : 用 git config --global 写
* 
* git config --global user.name yang
* git config --global user.email "liuyang_024@sina.com"
* git config --global core.editor vim
* git config --global merge.tool vimdiff
* git config --list
* git config user.name
* 
* git clone <url> : 克隆远程仓库
* git init : 初始化 git 项目
* git add : 将文件添加到 repo 中
* git add . : 将当前文件夹中的全部文件加入缓冲区
* git status : 查看 modify 的状态
* git diff : 对比修改的文件和暂存区(staging)内容 
* git diff --cached : 对比暂存区文件和上次提交快照之间的差异
* git diff --staged : 同上
* git commit : 提交(稍后会打开 editor 进行询问添加注释)
* git commit -v : 提交 change 并包括所有改动的细节
* git commit -m 'comment' : 一行提交方式
* git commit -a : 强制提交所有 modify 的 change (跳过 git add 的环节)
* git commit —amend : 修改最近一条提交消息
* git rm <file> : 推送删除文件到缓冲区 (首先要删除文件)
* git rm -f : 强制推送删除文件到缓冲区 (当删除之前已经推送UPD到缓冲区)
* git rm --cached <file> : 从仓库中删除文件, 但不会删除本地的文件
* git rm log/\*.log : 推送指定删除文件 Pattern 到 staging
* git mv <file_from> <file_to> : 对文件重命名 (= mv + git rm + git add )
* git log : 查看 log, 并按照提交时间列出所有更新
* git log -p : 显示 log 的更改详细信息
* git log -2 : 显示最近更改的2次更新
* git log --word-diff : 显示 log 并进行单词层面的对比(对程序代码没有意义)
* git log -U1 : 显示 log, 并且更改上下文由默认3行减为1行
* git log --stat : 显示简要的更改行数统计
* git log --pretty=oneline : 一行展示每次更新 log
* git log --pretty=short : 
* git log --pretty=full :  
* git log --pretty=fuller : 
* git log --pretty=format:"%h - %an, %ar : %s" : 指定格式展示 log
* git log --graph : 显示 log 并加入形象化分支情况
* git log --since=2.weeks :
* git log --until :
* git log --author : 显示 log 并指定作者
* git log --since=2.weeks --author=yang --all-match : 匹配所有项
* gitk : 显示 git log 的图形化界面
* git commit --amend : 修改最后一次提交 (可补充 change 或者 修改 comment)
* git reset HEAD <file> : 取消某个文件的 staging 状态
* git checkout -- <file> : 取消修改 (修改内容会被覆盖成原内容)
* git remote : 查看远程仓库
* git remote -v : 远程仓库详细信息 (--verbose)
* git remote add <shortname> <url> : 添加远程仓库
* git fetch <remote-name> : 抓取远程仓库仓库中本地还没有的内容
* * 只是会将远程数据放到本地仓库, 不会进行 merge
* * 并且 origin/master 会指向最新的远程 master 位置
* git pull : 自动抓取数据从远程仓库并且 merge 到当前分支
* git push <remote-name> <branch-name> : 将本地仓库数据推送到远程仓库
* git remote show <remote-name> : 显示远程仓库具体信息	
* git remote rename pb pual : 将本地的远程仓库名字修改 pb->pual
* git remote rm pual : 删除远程仓库 pual
* git tag : 列出所有的标签
* git tag -l "v1.4.2.*" : 列出指定Pattern的标签
* git tag v1.4 : 创建轻量级标签
* git tag -a v1.4 -m "my version 1.4" : 创建含 annotation 的标签
* git show v1.4 : 查看标签版本信息, 并连同显示打标签时的提交信息
* git tag -a v1.2 9fceb02 : 补充标签 (后跟上提交对象的校验和前几位字符即可)
* git push origin <tagname> : 推送标签
* git push origin --tags : 推送所有标签

### Pro Git - 分支 ###

HEAD 为当前分支的别名, 默认创建 master 分支

* git branch : 列出所有分支
* git branch testing : 创建 testing 分支
* git branch -d hotfix : 删除 hotfix 分支
* git branch -D hotfix : 强制删除 hotfix 分支
* git branch -v : 查看分支最后一个提交对象信息
* git branch --merged : 列出已经与当前 merged 的分支
* git branch --no-merged : 列出没与当前分支 merged 的分支
* git checkout testing : 切换到 testing 分支
* git checkout -b 'iss53' : 新建并且切换到 iss53 分支
* git checkout -b serverfix origin/serverfix : 基于 origin/serverfix 创建 serverfix 分支, 并切换到 serverfix 分支
* git checkout --track origin/serverfix : 同上
* git checkout -b sf origin/serverfix : 基于 origin/serverfix 创建 sf 分支, 并切换到 sf 分支
* git merge hotfix : 将 hotfix 改动 merge 到当前 branch
* git merge origin/serverfix : 将 origin/serverfix 内容 merge 进来
* git mergetool : 启动 mergetool 查看冲突并且编辑
* git push origin serverfix : 推送分支 serverfix 到 origin 远程仓库
* git push origin serverfix:awesomebranch : 推送分支到 origin 远程仓库并命名为 awesomebranch
* (远程分支中) git push : 会自行推断向哪个服务器哪个分支推送数据
* (远程分支中) git pull : 会获取所有远程索引, 并把他们的数据都合并到本地分支中
* git push origin :serverfix : 删除远程 serverfix 分支 (比较无厘头, 相当于提取本地空白到远程 serverfix 分支中)
* git rebase master : 将当前分支与 master 分支合并成新节点, master 作父亲节点 
* * 提交历史也会改写, 应用所有 master 上的新补丁. 
* * 相比 merge 具有更完整整洁的提交历史. 
* * 复杂 merge 的责任交给需要 rebase 分支的人
* * 维护 master 的人不需要做复杂 merge!! 只会快进 merge
* git rebase --onto master server client : 取出 client 分支, 找出 client 分支和 server 分支的共同祖先之后的变化, 然后再 master 上重演一遍
* git rebase master server : 将 server 分支衍合到 master 上 (无需手动切换到 server 分支)
* merge 后推荐快进 merge master 分支
* * git checkout master
* * git merge server

!!! 一旦分支中的提交对象发布到了公共仓库, 就千万不要对该分支进行 rebase 操作
!!! 因为 parent base 修改了丢弃了原 base, 所以没办法愉快的让别人合并了